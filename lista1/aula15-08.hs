-- Problema do maior elemento de uma lista
-- horrivel
maior [x] = x
maior (x:xs) = if x > (maior xs)
      then x
      else (maior xs)

-- possivel solucao
maiorBom [x] = x
maiorBom (x:xs) = maux x (maiorBom xs)

maux a b = if a>b then a else b

-- sintaxe ifelse
maior [x] = x
maior(x:xs)
    | x > maior xs = x
    | otherwise = maior xs

-- variaveis locais
maior [x] = x
maior (x:xs) = if x>mm then x else maior
    where mm = maior xs

-- recursao com acumulador

soma [] acc = acc
soma (x:xs) acc = soma xs (acc+x) -- faz primeiro a conta