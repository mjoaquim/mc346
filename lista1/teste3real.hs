data Tree ch v = Vazia | No ch v (Tree ch v) (Tree ch v) deriving (Eq,Show,Read)

t = No 5 2 (No 3 2 Vazia Vazia) (No 9 2 Vazia Vazia)

insereAbb :: (Ord ch) => ch -> v -> Tree ch v -> Tree ch v
insereAbb ch v Vazia = No ch v Vazia Vazia
insereAbb ch v (No key value Vazia Vazia) = if key == ch 
    then No key v Vazia Vazia
    else  if ch < key 
        then No key value (No ch v Vazia Vazia) Vazia
        else No key value Vazia (No ch v Vazia Vazia)
insereAbb ch v (No key value esq dir) = if key == ch 
    then No key v esq dir
    else if ch < key 
        then No key value (insereAbb ch v esq) dir
        else No key value esq (insereAbb ch v dir)


