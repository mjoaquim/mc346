contait it lista = contait' it lista 0
    where contait' _ [] acc = acc
          contait' it (x:xs) acc
            | x == it = contait' it xs (acc+1)
            | otherwise = contait' it xs acc