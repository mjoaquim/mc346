posicoes:: (Eq a, Num b) => a -> [a] -> [b]

posicoes _ [] = []
posicoes a l = posicoes' a l 0 []
    where posicoes' _ [] _ acc = acc
          posicoes' a (x:xs) i acc 
            | x == a = i:posicoes' a xs (i+1) acc
            | otherwise = posicoes' a xs (i+1) acc