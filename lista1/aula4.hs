-- Lazy Valuation  (gera lsita [1..4] => [1,2,3,4])
let a = [3..] -- (lista infinita) nao gera a lista, cria a promessa
-- so quando executa, ele gera a promessa

-- Mas se usarmos a funcao
pega 0 _ = []
pega n (x:xs) = x:(pega (n-1) xs)
-- saida [1,2,3]

tira 0 x = x
tira n (x:xs) = tira (n-1)xs

dupl [] = []
dup (x:xs) = (2*x):dup xs


-- criando seus proprios tipos
data Ponto = Ponto Float Float deriving (Eq, Read, Show)
data Figura = Circulo Ponto Float

area (Circulo _ r) = r * 3.14 * r
area (Retangulo (Ponto xa ta) (Ponto xb yb)) = abs ((ya-yb)*(xa-xb))


-- arvores
data Tree a = Vazia | No a (Tree a) (Tree a) deriving(Eq,Show,Read)


---- exemplo

data Ponto = Ponto Float Float deriving (Eq, Read, Show)
distancia (Ponto x y) (Ponto a b) =
        sqrt(dx*2 +dy**2)
            where dx = x - a 
                  dy = y - b

let a = Ponto 0 0
let b = Ponto 10 (-10)
distancia a b


---- exercicos 
-- achar u