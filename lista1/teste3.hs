data Tree ch v = Vazia | No ch v (Tree ch v) (Tree ch v) deriving (Eq,Show,Read)

t = No 5 2 (No 3 2 Vazia Vazia) (No 9 2 Vazia Vazia)

insereAbb :: (Ord ch) => ch -> v -> Tree ch v -> Tree ch v
insereAbb ch v Vazia = No ch v Vazia Vazia
insereAbb ch v (No key value Vazia Vazia)
    | key == ch = No key v Vazia Vazia
    | ch < key = No key value (No ch v Vazia Vazia) Vazia
    | otherwise = No key value Vazia (No ch v Vazia Vazia)
insereAbb ch v (No key value esq dir)
    | key == ch = No key v esq dir
    | ch < key = No key value (insereAbb ch v esq) dir
    | otherwise = No key value esq (insereAbb ch v dir)