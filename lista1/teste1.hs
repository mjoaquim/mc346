-- posicoes - dado um item e uma lista, retorna uma lista com todas as posicoes (primeiro elemento esta na posicao 1) do item na lista
posicoes item lista = posicoes' item lista 1
    where posicoes' _ [] _ = []
          posicoes' item (x:xs) acc
            | x == item = (acc+1):posicoes' item xs (acc+1)
            | otherwise = posicoes' item xs (acc+1)

-- troca velho novo
troca velho novo [] = []
troca velho novo x:xs
    | x == velho = novo:troca velho novo xs
    | otherwise = x:troca velho novo xs

-- split
split lista elemento = split' lista elemento []
    where split' [] _ acc = [acc]
          split' (x:xs) elemento acc
            | elemento == x = acc:[xs]
            | otherwise = split' xs elemento (acc+[xs]) 

-- splitall
splitall lista elemento = splitall' lista elemento []
    where splitall' [] _ acc = [acc]
          splitall' (x:xs) elemento acc 
            | x == elemento = acc:splitall' xs elemento []
            | otherwise = splitall' xs elemento (acc+[xs]) 

-- drop
coco n [] = []
coco 0 lista = lista
coco n (x:xs) = coco (n-1) xs


-- maior elemento lista
maior [] = error "empty list"
maior [x] = x
maior (x:xs)
  | x >= mm = x
  | otherwise = mm
  where mm = maior xs








