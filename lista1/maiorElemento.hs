sumElements l = if l == [] 
    then 0
    else (head l) + sumElements (tail l)


sumEvenElements l = if l == [] 
    then 0
    else if head l `mod` 2 == 0
        then head l + sumEvenElements (tail l)
        else sumEvenElements (tail l)
    
lastElement l = if l == []  -- not done yet
    then 0
    else head l lastElement (tail l)
    

existNumberX l x = if l == [] 
    then False
    else if head l == x then True
                        else existNumberX (tail l) x

factorial x = if x == 0
    then 1
    else x*factorial(x-1)

factorialPrint x = if x == 0
    then 1
    else (x factorialPrint(x-1))



freqNumber x l = if l == []
        then 0
        else if(head l == x) 
             then 1 + freqNumber x tail l 
             else freqNumber x tail l
        